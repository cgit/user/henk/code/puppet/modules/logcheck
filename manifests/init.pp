class logcheck (
  String $reportlevel = 'server'
) {
  package {
    ['logcheck','logcheck-database']:
      ensure        => installed,
    ;
  }
  file {
    '/etc/logcheck/logcheck.conf':
      ensure  => present,
      content => template('logcheck/etc/logcheck/logcheck.conf.erb'),
    ;
    '/etc/logcheck/ignore.d.server/':
      ensure  => directory,
      source  => 'puppet:///modules/logcheck/etc/logcheck/ignore.d.server/',
      recurse => remote,
      replace => true,
    ;
    '/etc/logcheck/ignore.d.workstation/':
      ensure  => directory,
      source  => 'puppet:///modules/logcheck/etc/logcheck/ignore.d.workstation/',
      recurse => remote,
      replace => true,
    ;
    '/etc/logcheck/violations.ignore.d/':
      ensure  => directory,
      source  => 'puppet:///modules/logcheck/etc/logcheck/violations.ignore.d/',
      recurse => remote,
      replace => true,
    ;
  }
  File <| tag == 'logcheck-logfile' |>
}
